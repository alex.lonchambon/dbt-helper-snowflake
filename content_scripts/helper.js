let models, isEnabled = true, isDebug = false;

chrome.storage.local.get("state").then(state => {
    models = state.models;
});

chrome.storage.local.get("enable").then(state => {
    isEnabled = state.enable === undefined ? isEnabled : state.enable;
});

chrome.storage.onChanged.addListener((changes) => {
    if (changes.hasOwnProperty("state")) models = changes.state.newValue.models;
    if (changes.hasOwnProperty("enable")) isEnabled = changes.enable.newValue;
})

window.addEventListener('keydown', async (event) => {
    if (isEnabled) {
        if (event.ctrlKey && event.key === 'q') {
            event.preventDefault();
            // console.log(event);
            navigator.clipboard.readText()
            .then(function(selectedText) {
                modifiedText = replaceSQL(selectedText, models);
                // Create a temporary input element of type "text"
                var tempTextArea = document.createElement("textarea");
                tempTextArea.value = modifiedText;
                
                // Append the temporary textarea element to the document
                document.body.appendChild(tempTextArea);
                
                // Select the text within the temporary textarea element
                tempTextArea.select();
                
                // Copy the selected text to the clipboard
                document.execCommand("copy");
            
                // Remove the temporary textarea element
                document.body.removeChild(tempTextArea);

            })
            // .catch(function(err) {
            //     console.error('Impossible de lire le presse-papiers : ', err);
            // });

        }
    }
});
